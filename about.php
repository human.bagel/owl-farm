<!DOCTYPE html>
<html>
<head>
<title>Owl Farm - About</title>
<link rel="stylesheet" href="style.css">
<?php require("includes/meta.html"); ?>
</head>
<body>
<h1>About Owl Farm</h1>
<?php include("includes/menu.html"); ?>
<p><b>Owl Farm is an anonymous, free, instant, encrypted and secure temporary information service.</b></p>
<p><b>Completely Anonymous</b>. No logs are kept, <b>no ip addresses are logged</b>, the entire site is <b>secured with HTTPS.</b></p>
<p>Totally <b>Free!</b> There are <b>no advertisements</b> or tracking. <b>No cookies.  No registration</b> is required or allowed.</p>
<p>We do not have any data retention, forced or otherwise.  We don't even have a hit counter.</p>
<h2>Secure Video Chat</h2>
<p>Video Chat is <b>secure, peer to peer, encrypted</b>, and <b>completely anonymous</b>.  Keep in mind this is in beta.</p>
<p>Owl Farm Video Chat is the most <b>secure, anonymous</b> and <b>private</b> video chat available.</p>
<p>Your video and audio are <b>never routed through this server</b>, it is purely <b>peer to peer</b> and the server only exists to securely exchange metadata.</p>
<p><b>NO PLUGINS OR FLASH REQUIRED, PURE HTML5</b></b>
<p>Only <b>two people to a room</b> currently.</p>
<p><b>Create a room</b> name or <b>join one</b> already created.</p>
<h2>Unencrypted File Sharing</h2>
<p>Owl Farm offers traditional <b>24 hour file</b> sharing based on <b><a href="https://github.com/nokonoko/Uguu">uguu</a></b></p>
<p>After <b>24 hours</b> all files are <b>securely shredded</b>.  Cryptographically secure URLs are extremely hard to guess and <b>can not traced back to a user.</b>

<h2>ShareX Integration</h2>
<p>For easy file/text/screenshot sharing try <a href="https://getsharex.com/">ShareX</a> and use <a href="owlfarm.json">this json file</a> to configure a custom uploader.</p>

<h2>Encrypted File Sharing</h2>

<p>Files are only stored for <b>24 hours</b>, after that they are <b>securely shredded.</b></p>
<p>Files uploaded are <b>encrypted in-browser</b> before being sent to the server.  We don't have the password for the file and can't access it.</p>

<p>Note: This is very much a work in progress. Known issues:</p>
<ul>
<li>Files larger than 3mb fail</li>
<li>Flaky performance</li>
<li>No proper error message on bad password</li>
<li>When a password is too short refresh is required</li>
<li><b>Does not work in Internet Explorer or Safari (tested in Fire Fox and Chrome)</b></li>
</ul>

<h2>IP Check</h2>
<p>IP Check returns your public IP, all the headers sent, and an attempt at locating your location by your IP.</p>
<br>
  
<h2>Proxy List</h2>
<p>A list of active SOCKS5 high anonymity proxies in a format that can be used as a proxy subscription.</p>
<p>Tests proxies from 50+ sources via an implementation of <b><a href="http://proxybroker.readthedocs.io/en/latest/">ProxyBroker</a></b></p>
<p>Generates 35 fresh proxies every 10 minutes.</p>
<p><em>We have not been served any secret court orders and are not under any gag orders.</em></p>
<p><a href="index.php">Upload a file</a></p>
</body>
</html>