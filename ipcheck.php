<!DOCTYPE HTML>
<html>
	<head>
		<title>IP Check</title>
		<link rel="stylesheet" href="style.css">
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta name="robots" content="noindex, nofollow">
		<title>IP CHECK</title>
	</head>
<body>
<h1>Owl Farm IP Check</h1>
<?php include("includes/menu.html"); ?>
<p>Every shred of information you send to this web server.</p>
<?php
function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
     $ip=$_SERVER['HTTP_CLIENT_IP'];
	}  
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

getRealIpAddr();
$details = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.getRealIpAddr()));

$ip = $details['geoplugin_request'];
$city = $details['geoplugin_city'];
$state = $details['geoplugin_regionName'];
$country = $details['geoplugin_countryName'];

echo "<div id=\"personalinfo\"><p><b>Your IP address: <i>$ip</i></b></p><p>Your location {$state}, {$country} near $city</div>";

$SERVER = $_SERVER;
unset($SERVER['USER']);
unset($SERVER['HOME']);
unset($SERVER['SCRIPT_FILENAME']);
unset($SERVER['DOCUMENT_ROOT']);
unset($SERVER['SERVER_SOFTWARE']);
unset($SERVER['REQUEST_TIME_FLOAT']);
unset($SERVER['REQUEST_TIME']);
//unset($SERVER['RCGI_ROLE']);
unset($SERVER['FCGI_ROLE']);
unset($SERVER['GATEWAY_INTERFACE']);
unset($SERVER['PHP_SELF']);

echo '<pre>RAW HEADERS
';
print_r($SERVER);
echo '</pre>';
?>
</body>
</html>